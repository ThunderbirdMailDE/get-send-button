***Get/Send Button***

--------

### Features

* This Thunderbird Add-on provides a combined Get and Send button with a context menu with additional features.

### Version series

* Version 8.*    - Thunderbird 102.* - Migration to a MailExtension - in development
* Version 7.*    - Thunderbird 91.*
* Version 6.*    - Thunderbird 78.*
* Version 5.7.*  - Thunderbird 68.*
* Version 5.3.*  - Thunderbird 52.0 - 60.*

### Known issues

* 

### Installation

1. [Download Allow HTML Temp from the official Thunderbird add-on page](https://addons.thunderbird.net/addon/getsendbutton/)
2. [Installing an Add-on in Thunderbird](https://support.mozilla.org/kb/installing-addon-thunderbird)


### Contribution

You are welcomed to contribute to this project by:
* adding or improving the localizations of AttachmentExtractor Continued via email to me or a post in german Thunderbird forums [Thunderbird Mail DE](https://www.thunderbird-mail.de/forum/board/81-hilfe-und-fehlermeldungen-zu-thunders-add-ons/) or just create an [issue](https://gitlab.com/ThunderbirdMailDE/get-send-button/issues/) here on GitLab
* creating [issues](https://gitlab.com/ThunderbirdMailDE/get-send-button/issues/) about problems
* creating [issues](https://gitlab.com/ThunderbirdMailDE/get-send-button/issues/) about possible improvements


### Coders

* Alexander Ihrig (Original Author and Maintainer)

### Translators

* 


### License

[Mozilla Public License version 2.0](https://gitlab.com/ThunderbirdMailDE/get-send-button/LICENSE)