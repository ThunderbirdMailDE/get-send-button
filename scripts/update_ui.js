// Listen for option changes to set the button icon and label
async function registerListeners() {
  await messenger.storage.onChanged.addListener(setButtonIconAndLabel);
  consoleDebug("GetSendButton: registered initial listeners for update_ui.js: ");
}

async function setButtonIconAndLabel() {
  // buttonIcon = {};
  buttonLabel = {};

  await reloadOption("sendYes");
  consoleDebug("GetSendButton: setButtonIconAndLabel: options.sendYes: " + options.sendYes);
  await reloadOption("onlySingleAccount");
  consoleDebug("GetSendButton: setButtonIconAndLabel: options.onlySingleAccount: " + options.onlySingleAccount);
  await reloadOption("askPasswords");
  consoleDebug("GetSendButton: setButtonIconAndLabel: options.askPasswords: " + options.askPasswords);

  switch (options.sendYes) {
    case "sendYes":
      consoleDebug("GetSendButton: setButtonIconAndLabel: buttonLabel.label: sendYes");
      buttonLabel.label = browser.i18n.getMessage("buttonGetAndSend");
      break;
    case "sendNo":
      consoleDebug("GetSendButton: setButtonIconAndLabel: buttonLabel.label: sendNo");
      buttonLabel.label = browser.i18n.getMessage("buttonGetOnly");
      break;
    default:
      consoleDebug("GetSendButton: setButtonIconAndLabel: buttonLabel.label: Default");
      buttonLabel.label = browser.i18n.getMessage("buttonGetAndSend");
  }

  /* only needed, if different icons would be used
  switch (options.onlySingleAccount) {
    case "getSingle":
      consoleDebug("GetSendButton: setButtonIconAndLabel: buttonIcon.path: getSingle");
      // buttonIcon.path = "../icons/getsend_fill_context.svg";
      break;
    case "getAll":
      consoleDebug("GetSendButton: setButtonIconAndLabel: buttonIcon.path: getAll");
      // buttonIcon.path = "../icons/getsend_fill_context.svg";
      break;
    default:
      consoleDebug("GetSendButton: setButtonIconAndLabel: buttonIcon.path: Default");
      // buttonIcon.path = "../icons/getsend_fill_context.svg";
  }
  */

  /* Button als browserAction noch nicht implementiert
  // browser.browserAction.setIcon(buttonIcon);
  browser.browserAction.setLabel(buttonLabel);
  */
}

registerListeners();
setButtonIconAndLabel();
