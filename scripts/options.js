var options = {};

async function optionsMigrate() {
  // Because of old LegacyPrefs using other value then the new options system, we can't do a simple recursion for the 3 LegacyPrefs.
  // Therefore the following procedures (only) seem to be redundant.

  let legacyName = "";
  let legacyValue = null;

  legacyName = "extensions.getsendbutton.GetSendButton_SendYes";
  legacyValue = null;
  legacyValue = await messenger.LegacyPrefs.getUserPref(legacyName);

  if (legacyValue !== null) {
    console.log("Migrating legacy preference: " + legacyName + " = ", legacyValue);

    // Migrate old boolean value to new string value
    legacyValue = ((legacyValue == true) ? "sendYes" : "sendNo");
    messenger.storage.local.set({
      sendYes: legacyValue
    });

    // Clear the legacy value.
    // messenger.LegacyPrefs.clearUserPref(legacyName);
  }

  legacyName = "extensions.getsendbutton.GetSendButton_OnlySingleAccount";
  legacyValue = null;
  legacyValue = await messenger.LegacyPrefs.getUserPref(legacyName);

  if (legacyValue !== null) {
    console.log("Migrating legacy preference: " + legacyName + " = ", legacyValue);

    // Migrate old boolean value to new string value
    legacyValue = ((legacyValue == true) ? "getSingle" : "getAll");
    messenger.storage.local.set({
      onlySingleAccount: legacyValue
    });

    // Clear the legacy value.
    // messenger.LegacyPrefs.clearUserPref(legacyName);
  }

  legacyName = "extensions.getsendbutton.GetSendButton_AskPasswords";
  legacyValue = null;
  legacyValue = await messenger.LegacyPrefs.getUserPref(legacyName);
  if (legacyValue !== null) {
    console.log("Migrating legacy preference: " + legacyName + " = ", legacyValue);

    messenger.storage.local.set({
      askPasswords: legacyValue
    });

    // Clear the legacy value.
    // messenger.LegacyPrefs.clearUserPref(legacyName);
  }
}

async function optionsInit() {
  await reloadAllOptions();
}

async function reloadAllOptions() {
  await reloadOption("debug");
  await reloadOption("sendYes");
  await reloadOption("onlySingleAccount");
  await reloadOption("askPasswords");

  // Migrate options values over to LegacyPrefs, to be able to use them in still existing WL API extension part
  // Because of only using these 3 prefs in an own extensions.getsendbutton pref branch, 
  // there should no issues occur within Thunderbird.
  await browser.LegacyPrefs.setPref("extensions.getsendbutton.GetSendButton_SendYes", ((options.sendYes === "sendYes") ? true : false));
  await browser.LegacyPrefs.setPref("extensions.getsendbutton.GetSendButton_OnlySingleAccount", ((options.onlySingleAccount === "getAll") ? false : true));
  await browser.LegacyPrefs.setPref("extensions.getsendbutton.GetSendButton_AskPasswords", ((options.askPasswords == true) ? true : false));
}

function reloadOption(id) {
  return messenger.storage.local.get(id).then((res) => {
    if (res[id] != undefined)
      options[id] = res[id];
    else
      options[id] = DefaultOptions[id];
  }, defaultError);
}
