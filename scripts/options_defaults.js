const DefaultOptions = {
  debug: false,
  sendYes: "sendYes",
  onlySingleAccount: "getAll",
  askPasswords: true
}
const OptionsList = Object.keys(DefaultOptions);

function defaultError(error) {
  console.error("GetSendButton: Error:", error);
}